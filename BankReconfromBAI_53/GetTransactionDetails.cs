﻿using System;

namespace BankReconfromBAI_53
{
    class GetTransactionDetails : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        //properties
        public string Rec88 { set; get; }
        public string AcctUnit { set; get; }
        public string DistAcct { set; get; }
        public string SubAcct { set; get; }
        public string BaiCode { set; get; }

        //method
        public void UnScramble88()
        {
            string tmp = string.Empty;
            SubAcct = string.Empty;
            switch (BaiCode)
            {
                case "108":
                    DistAcct = "51750";
                    AcctUnit = "900000";
                    break;
                case "142":
                    DistAcct = "13002";        //per Sabrina 9/26/2016 "51750";
                    SubAcct = "7435";
                    break;
                case "172":
                    DistAcct = "13002";        //per Sabrina 9/29/2016 "51750";
                    SubAcct = "7435";
                    break;
                case "275":
                    break;
                case "301":
                    if (AcctUnit.Trim().Length == 0 || AcctUnit.Trim().Length >4)
                    {
                        AcctUnit = "900000";
                        DistAcct = "900000";
                    }
                    else 
                    {
                        if (Rec88.Contains("DEPOSIT"))
                        {
                            DistAcct = "13002";
                            SubAcct = "7435";
                        }
                        else
                        {
                            DistAcct = "10301";
                            SubAcct = string.Empty;
                        }
                    }
                    break;
                case "357":
                    DistAcct = "13002";        //added per Sabrina 7/31/2017 
                    SubAcct = "7435";
                    break;
                case "399":
                    DistAcct = "13002";
                    SubAcct = "7435";
                    break;
                case "409":
                    DistAcct = "51750";
                    AcctUnit = "900000";
                    break;
                case "451":
                    DistAcct = "10099";
                    AcctUnit = "900000";
                    break;
                case "575":
                    break;
                case "631":
                    if (Rec88.ToUpper().Contains("FUNDS TRANSFER DEBIT"))
                    {
                        DistAcct = "13002";
                        SubAcct = "7435";
                        AcctUnit = "9999"; // AMB 1/2/2018
                    }
                    else
                    {
                        DistAcct = "99999";
                        AcctUnit = "900000";
                    }
                    break;
                case "666":
                    DistAcct = "10301";
                    // per Marc 7/27/2016       SubAcct = "7435";
                    break;
                case "695":
                    DistAcct = "51750";
                    break;
                case "698":
                    DistAcct = "51700";
                    AcctUnit = "900000";
                    break;
                case "699":     //per Sabrina cause by fifth/third screw-up
                    DistAcct = "13002";
                    SubAcct = "7435";
                    break;
                default:
                    break;
            }
            tmp = AcctUnit;
            if (tmp =="12474")
            {
                tmp = "900000";
            }
            if (tmp.Length > 0)
            {
                using (CodeSQL clsSql = new CodeSQL())
                {
                    switch (tmp.Length)
                    {
                        case 1:
                            tmp = "0010" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            break;
                        case 2:
                            tmp = "001" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            break;
                        case 3:
                            tmp = "00" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            break;
                        case 4:
                            tmp = $"{tmp}00";
                            break;
                        case 5:
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            break;
                        default:
                            break;
                    }
                }
                //using (CodeSqlCe clsSqlCe = new CodeSqlCe())
                //{
                //    switch (tmp.Length)
                //    {
                //        case 1:
                //            tmp = "0010" + tmp;
                //            tmp =
                //                clsSqlCe.ExecuteScalar(
                //                    $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                //            break;
                //        case 2:
                //            tmp = "001" + tmp;
                //            tmp =
                //                clsSqlCe.ExecuteScalar(
                //                    $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                //            break;
                //        case 3:
                //            tmp = "00" + tmp;
                //            tmp =
                //                clsSqlCe.ExecuteScalar(
                //                    $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                //            break;
                //        case 4:
                //            tmp = $"{tmp}00";
                //            break;
                //        default:
                //            break;
                //    }
                //}
            }
            AcctUnit = tmp;
        }
    }
}
